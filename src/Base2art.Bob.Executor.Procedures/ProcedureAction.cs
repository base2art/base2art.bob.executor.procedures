﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    
    public abstract class ProcedureAction<TParams> : Procedure<TParams>
        where TParams : IProcedureParameters
    {
        protected sealed override IProcedureExecutionResult ExecuteProcedure(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            TParams parameters,
            ProcedureLogger procedureLogger,
            VariableData exportedData)
        {
            this.ExecuteProcedureAction(serviceLocator, targetExecutionParameters, parameters, procedureLogger, exportedData);
            return new ProcedureExecutionResult(true, this.ConvertVariables(exportedData));
        }

        protected abstract void ExecuteProcedureAction(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            TParams parameters,
            ProcedureLogger procedureLogger,
            VariableData exportedData);
    }
}
