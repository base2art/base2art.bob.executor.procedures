﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;

    [Flags]
    public enum TargetExecutionStatus
    {
        Success                                 = 0x0001,
        Failure                                 = 0x0010,
        SuccessOrFailure                        = 0x0011,
        Abort                                   = 0x0100,
        SuccessOrAbort                          = 0x0101,
        FailureOrAbort                          = 0x0110,
        SuccessOrFailureOrAbort                 = 0x0111,
        Inconclusive                            = 0x1000,
        SuccessOrInconclusive                   = 0x1001,
        FailureOrInconclusive                   = 0x1010,
        SuccessOrFailureOrInconclusive          = 0x1011,
        AbortOrInconclusive                     = 0x1100,
        SuccessOrAbortOrInconclusive            = 0x1101,
        FailureOrAbortOrInconclusive            = 0x1110,
        SuccessOrFailureOrAbortOrInconclusive   = 0x1111,
    }
}
