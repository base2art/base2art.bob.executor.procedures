﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    
    public class ProcedureExecutionResult : IProcedureExecutionResult
    {
        private readonly IVariableDataTree exportedData;

        private readonly bool? isSuccessRun;
        
        public ProcedureExecutionResult(bool? isSuccessRun, IVariableDataTree exportedData)
        {
            if (exportedData == null)
            {
                throw new ArgumentNullException("exportedData");
            }
            
            this.isSuccessRun = isSuccessRun;
            this.exportedData = exportedData;
        }
        
        public bool? IsSuccessfulRun
        {
            get { return this.isSuccessRun; }
        }

        public IVariableDataTree ExportedData
        {
            get { return this.exportedData; }
        }
    }
}
