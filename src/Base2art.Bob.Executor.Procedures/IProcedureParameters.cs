﻿namespace Base2art.Bob.Executor.Procedures
{
    public interface IProcedureParameters
    {
        string Description { get; }
    }
}
