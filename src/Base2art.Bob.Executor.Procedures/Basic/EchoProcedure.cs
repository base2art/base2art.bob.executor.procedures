﻿namespace Base2art.Bob.Executor.Procedures.Basic
{
    [DependencyIdentifier("Base2art", "Echo", "1.0.0.0")]
    public class EchoProcedure : Procedure<EchoProcedureParameters>
    {
        protected override IProcedureExecutionResult ExecuteProcedure(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters, 
            EchoProcedureParameters parameters, 
            ProcedureLogger procedureLogger,
            VariableData exportedData)
        {
            procedureLogger.Write(parameters.Message ?? "<null>");
            return new ProcedureExecutionResult(true, this.ConvertVariables(exportedData));
        }
    }
}
