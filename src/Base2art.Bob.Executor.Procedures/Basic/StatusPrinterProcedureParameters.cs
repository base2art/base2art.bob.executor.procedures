﻿namespace Base2art.Bob.Executor.Procedures.Basic
{
    [DependencyIdentifier("Base2art", "Status-Printer-Parameters", "1.0.0.0")]
    public class StatusPrinterProcedureParameters : ProcedureParameters
    {
        public StatusPosition StatusPosition { get; set; }
    }
}
