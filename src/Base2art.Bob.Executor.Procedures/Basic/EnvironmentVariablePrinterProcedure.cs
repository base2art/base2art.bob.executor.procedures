﻿namespace Base2art.Bob.Executor.Procedures.Basic
{
    using System.Collections.Generic;
    using System.Globalization;
    
    [DependencyIdentifier("Base2art", "Environment-Variables-Printer", "1.0.0.0")]
    public class EnvironmentVariablePrinterProcedure : ProcedureAction<NullProcedureParameters>
    {
        protected override bool CanExecuteProcedure(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            NullProcedureParameters parameters,
            ProcedureLogger procedureLogger)
        {
            return true;
        }
        
        protected override void ExecuteProcedureAction(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            NullProcedureParameters parameters,
            ProcedureLogger procedureLogger,
            VariableData exportedData)
        {
            WriteFormatted(
                procedureLogger,
                "TargetExecutionParameters.DefaultWorkingDirectory: '{0}'",
                targetExecutionParameters.DefaultWorkingDirectory);
        }

        private static void WriteFormatted(ProcedureLogger procedureLogger, string formatString, string value)
        {
            var message = string.Format(CultureInfo.CurrentUICulture, formatString, value);
            procedureLogger.Write(message);
        }
    }
}
