﻿namespace Base2art.Bob.Executor.Procedures.Basic
{
    [DependencyIdentifier("Base2art", "Null", "1.0.0.0")]
    public class NullProcedure : ProcedureAction<NullProcedureParameters>
    {
        protected override void ExecuteProcedureAction(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters, 
            NullProcedureParameters parameters, 
            ProcedureLogger procedureLogger,
            VariableData exportedData)
        {
        }
    }
}
