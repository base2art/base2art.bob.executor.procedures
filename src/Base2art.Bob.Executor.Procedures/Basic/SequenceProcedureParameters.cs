﻿namespace Base2art.Bob.Executor.Procedures.Basic
{
    using Base2art.Bob.Executor.Procedures.CustomTypes;
    
    [DependencyIdentifier("Base2art", "Sequence-Parameters", "1.0.0.0")]
    public class SequenceProcedureParameters : ProcedureParameters
    {
        public ProcedureExecutionData[] Procedures { get; set; }
    }
}
