﻿namespace Base2art.Bob.Executor.Procedures.Basic
{
    using System.Collections.Generic;
    using System.Linq;
    
    [DependencyIdentifier("Base2art", "Sequence", "1.0.0.0")]
    public class SequenceProcedure : Procedure<SequenceProcedureParameters>
    {
        protected override IProcedureExecutionResult ExecuteProcedure(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            SequenceProcedureParameters parameters,
            ProcedureLogger procedureLogger,
            VariableData exportedData)
        {
            List<IVariableDataTree> childItems = new List<IVariableDataTree>();
            var status = targetExecutionParameters.Status;
            var position = targetExecutionParameters.CurrentLifecyclePosition;
            
            foreach (var x in parameters.Procedures)
            {
                targetExecutionParameters = new TargetExecutionParameters(
                    targetExecutionParameters.Id,
                    targetExecutionParameters.DefaultWorkingDirectory,
                    targetExecutionParameters.DefaultArtifactDirectory,
                    status,
                    position.ToLifecyclePosition());
                
                var result = x.ExecuteProcedure(
                    serviceLocator,
                    targetExecutionParameters,
                    procedureLogger);
                
                childItems.Add(result.ExportedData);
                
                if (result.GetStatus(targetExecutionParameters) == TargetExecutionStatus.Failure)
                {
                    status = TargetExecutionStatus.Failure;
                }
            }
            
            var tree = this.ConvertVariables(exportedData, childItems.ToArray());
            bool? isSuccess = null;
            if (status == TargetExecutionStatus.Failure)
            {
                isSuccess = false;
            }
            
            if (status == TargetExecutionStatus.Inconclusive || status == TargetExecutionStatus.Abort)
            {
                isSuccess = null;
            }
            
            if (status == TargetExecutionStatus.Success)
            {
                isSuccess = true;
            }
            
            return new ProcedureExecutionResult(isSuccess, tree);
        }
    }
}
