﻿namespace Base2art.Bob.Executor.Procedures.Basic
{
    using System.Collections.Generic;
    [DependencyIdentifier("Base2art", "Status-Printer", "1.0.0.0")]
    public class StatusPrinterProcedure : ProcedureAction<StatusPrinterProcedureParameters>
    {
        protected override bool CanExecuteProcedure(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters, 
            StatusPrinterProcedureParameters parameters, 
            ProcedureLogger procedureLogger)
        {
            return true;
        }
        
        protected override void ExecuteProcedureAction(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            StatusPrinterProcedureParameters parameters,
            ProcedureLogger procedureLogger,
            VariableData exportedData)
        {
            var ep = new StatusPrinterProcedureOutput();
            ep.StatusPosition = parameters.StatusPosition;
            ep.Status = targetExecutionParameters.Status;
            procedureLogger.Append(ep);
        }
    }
}
