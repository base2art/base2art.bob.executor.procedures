﻿namespace Base2art.Bob.Executor.Procedures.Basic
{
    using Base2art.Bob.Executor.Procedures.CustomTypes;

    [DependencyIdentifier("Base2art", "Parallel-Parameters", "1.0.0.0")]
    public class ParallelProcedureParameters : ProcedureParameters
    {
        public ProcedureExecutionData[] Procedures { get; set; }
        
        public int DegreeOfParallelism { get; set; }
    }
}
