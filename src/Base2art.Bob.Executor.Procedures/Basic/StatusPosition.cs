﻿namespace Base2art.Bob.Executor.Procedures.Basic
{
    public enum StatusPosition
    {
        LifecyclePosition,
        Begin,
        End
    }
}
