﻿namespace Base2art.Bob.Executor.Procedures.Basic
{
    public class StatusPrinterProcedureOutput
    {
        public StatusPosition StatusPosition { get; set; }

        public TargetExecutionStatus Status { get; set; }

        public LifecyclePosition LifecyclePosition { get; set; }
    }
}
