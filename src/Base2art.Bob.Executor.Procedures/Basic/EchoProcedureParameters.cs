﻿namespace Base2art.Bob.Executor.Procedures.Basic
{
    [DependencyIdentifier("Base2art", "Echo-Parameters", "1.0.0.0")]
    public class EchoProcedureParameters : ProcedureParameters
    {
        public string Message { get; set; }
    }
}
