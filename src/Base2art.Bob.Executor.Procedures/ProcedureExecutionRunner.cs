﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    using System.Globalization;
    using Base2art.Bob.Executor.Procedures.CustomTypes;
    
    public static class ProcedureExecutionRunner
    {
        public static IProcedureExecutionResult ExecuteProcedure(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetParameters,
            IProcedure procExecutable,
            object objectParameters,
            IProcedureLogger parentLogger)
        {
            try
            {
                return procExecutable.Execute(serviceLocator, targetParameters, objectParameters, new ProcedureLogger(parentLogger))
                    ?? new ProcedureExecutionResult(false, new VariableDataTree(DependencyIdentifierAttribute.Get(procExecutable)));
            }
            catch (AggregateException e)
            {
                Console.WriteLine(e.InnerException.Message);
                parentLogger.WriteDebug(e.InnerException.Message);
                return new ProcedureExecutionResult(false, new VariableDataTree(DependencyIdentifierAttribute.Get(procExecutable)));
            }
            catch (ProcessExecutionException e)
            {
                Console.WriteLine(e.Message);
                parentLogger.WriteDebug(e.Message);
                return new ProcedureExecutionResult(false, new VariableDataTree(DependencyIdentifierAttribute.Get(procExecutable)));
            }
            catch (ProcedureFailureException e)
            {
                Console.WriteLine(e.Message);
                parentLogger.WriteDebug(e.Message);
                return new ProcedureExecutionResult(false, new VariableDataTree(DependencyIdentifierAttribute.Get(procExecutable)));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                parentLogger.WriteDebug(e.Message);
                var lines = e.StackTrace.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var line in lines)
                {
                    parentLogger.WriteDebug(line);
                }
                
                return new ProcedureExecutionResult(false, new VariableDataTree(DependencyIdentifierAttribute.Get(procExecutable)));
            }
        }
        
        public static IProcedureExecutionResult ExecuteProcedure(
            this ProcedureExecutionData procedure,
            ProcedureServiceLocator locator,
            TargetExecutionParameters targetParameters,
            IProcedureLogger procedureLogger)
        {
            if (procedure == null)
            {
                throw new ProcedureFailureException("Invalid Procedure");
            }
            
            if (procedure.Procedure == null)
            {
                throw new ProcedureFailureException("Prodecure is null.");
            }
            
            if (procedure.ProcedureParameters == null)
            {
                throw new ProcedureFailureException("ProcedureParameters is null.");
            }
            
            var logger = procedureLogger == null
                ? locator.LoggerFactory.CreateLoggerFor(
                    targetParameters,
                    procedure.Procedure.ToDependencyIdentifier(),
                    procedure.ProcedureParameters.ToDependencyIdentifier())
                : locator.LoggerFactory.CreateLoggerFor(
                    targetParameters,
                    procedureLogger.Id,
                    procedure.Procedure.ToDependencyIdentifier(),
                    procedure.ProcedureParameters.ToDependencyIdentifier());
            
            if (logger == null)
            {
                throw new ProcedureFailureException("A developer messed up; the system failed to create a logger");
            }
            
            try
            {
                IProcedure procExecutable = locator.ProcedureLocator.GetProcedure(procedure.Procedure);
                
                if (procExecutable == null)
                {
                    var message = string.Format(
                        CultureInfo.InvariantCulture,
                        "Unable to find the type with id: '{0}:{1}:{2}'",
                        procedure.Procedure.Organization,
                        procedure.Procedure.Product,
                        procedure.Procedure.Version);
                    
                    logger.WriteError(message);
                    logger.AppendError(procedure);
                    throw new ProcedureFailureException(message);
                }
                
                var parameters = procedure.Parameters;
                var data = locator.Serializer.Serialize(parameters);
                var type = locator.DependencyIdentifierTypeLocator.FindType(procedure.ProcedureParameters);
                parameters = locator.Serializer.Deserialize(data, type);
                
                IProcedureExecutionResult result = ExecuteProcedure(
                    locator,
                    targetParameters,
                    procExecutable,
                    parameters,
                    logger);
                
                if (!result.IsSuccessfulRun.HasValue)
                {
                    return Return(logger, new ProcedureExecutionResult(null, result.ExportedData));
                }
                
                if (result.IsSuccessfulRun.HasValue && !result.IsSuccessfulRun.Value)
                {
                    return Return(logger, new ProcedureExecutionResult(false, result.ExportedData));
                }
                
                return Return(logger, new ProcedureExecutionResult(true, result.ExportedData));
            }
            catch (Exception)
            {
                logger.Flush(new ProcedureExecutionResult(false, new VariableDataTree(procedure.Procedure)));
                throw;
            }
        }
        
        public static TargetExecutionStatus GetStatus(
            this IProcedureExecutionResult result,
            TargetExecutionParameters targetExecutionParameters)
        {
            var status = targetExecutionParameters.Status;
            var originalStatus = targetExecutionParameters.Status;
            if (result.IsSuccessfulRun.HasValue)
            {
                status = result.IsSuccessfulRun.Value
                    ? TargetExecutionStatus.Success
                    : TargetExecutionStatus.Failure;
            }
            else
            {
                status = TargetExecutionStatus.Inconclusive;
            }
            
            if (originalStatus.HasFlag(TargetExecutionStatus.Failure) || originalStatus.HasFlag(TargetExecutionStatus.Abort))
            {
                status = originalStatus;
            }
            else if (originalStatus.HasFlag(TargetExecutionStatus.Inconclusive) && status != TargetExecutionStatus.Success)
            {
                status = originalStatus;
            }
            
            return status;
        }

        private static IProcedureExecutionResult Return(IProcedureLogger logger, ProcedureExecutionResult procedureExecutionResult)
        {
            logger.Flush(procedureExecutionResult);
            return procedureExecutionResult;
        }
    }
}
