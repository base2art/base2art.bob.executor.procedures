﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    
    public interface IProcedureLogger
    {
        /// <summary>
        /// Gets the id of the logger.
        /// </summary>
        Guid Id { get; }
        
        /// <summary>
        /// Should Not appear in output log.
        /// </summary>
        /// <param name="content">The debugging content.</param>
        void WriteDebug(string content);

        /// <summary>
        /// Should Show in task output log as standard output.
        /// </summary>
        /// <param name="content">The content to display to the end user.</param>
        void Write(string content);

        /// <summary>
        /// Should Show in task output log as standard output, but for the error stream.
        /// </summary>
        /// <param name="content">The content to display to the end user.</param>
        void WriteError(string content);

        /// <summary>
        /// Should Show in task output log as standard output.
        /// </summary>
        /// <param name="dataObject">The content to display to the end user.</param>
        void Append<T>(T dataObject);

        /// <summary>
        /// Should Show in task output log as standard output, but for the error stream.
        /// </summary>
        /// <param name="dataObject">The content to display to the end user.</param>
        void AppendError<T>(T dataObject);

        /// <summary>
        ///  This method flushes any internal buffers.
        /// </summary>
        void Flush(ProcedureExecutionResult result);
    }
}
