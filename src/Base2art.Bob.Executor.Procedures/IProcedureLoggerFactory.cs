﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    
    public interface IProcedureLoggerFactory
    {
        IProcedureLogger CreateLoggerFor(
            TargetExecutionParameters targetExecutionParameters,
            DependencyIdentifier procedure,
            DependencyIdentifier procedureParameters);
        
        IProcedureLogger CreateLoggerFor(
            TargetExecutionParameters targetExecutionParameters,
            Guid parentLoggerId,
            DependencyIdentifier procedure,
            DependencyIdentifier procedureParameters);
    }
}
