﻿namespace Base2art.Bob.Executor.Procedures
{
    using System.Linq;
    
    public sealed class VariableDataTree : IVariableDataTree
    {
        private readonly DependencyIdentifier identifier;
        
        private VariableData data;
        
        private VariableDataTree[] children;
        
        public VariableDataTree(DependencyIdentifier identifier)
        {
            if (identifier == null)
            {
                throw new System.ArgumentNullException("identifier");
            }
            
            this.identifier = identifier;
        }

        IDependencyIdentifier IVariableDataTree.Procedure
        {
            get { return this.identifier; }
        }
        
        public VariableData Data
        {
            get 
            {
                if (this.data == null) 
                {
                    this.data = new VariableData();
                }
                
                return this.data;
            }
            
            set
            {
                this.data = value; 
            }
        }

        public VariableDataTree[] Children
        {
            get 
            {
                if (this.children == null) 
                {
                    this.children = new VariableDataTree[0];
                }
                
                return this.children;
            }
            
            set
            {
                this.children = value; 
            }
        }
        
        IVariableData IVariableDataTree.Data
        {
            get { return this.Data; }
        }

        IVariableDataTree[] IVariableDataTree.Children
        {
            get { return this.Children; }
        }

        public VariableDataTree ToVariableTree()
        {
            var tree = new VariableDataTree(DependencyIdentifier.Create(this.identifier));
            tree.Children = this.Children.Select(x => x.ToVariableTree()).ToArray();
            tree.Data = this.Data.ToVariableData();
            
            return tree;
        }
    }
}
