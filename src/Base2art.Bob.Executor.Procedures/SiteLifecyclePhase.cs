﻿namespace Base2art.Bob.Executor.Procedures
{
    public enum SiteLifecyclePhase
    {
        None = 0,
        PreSite = 1,
        Site = 2,
        PostSite = 3,
        SiteDeploy = 4
    }
}
