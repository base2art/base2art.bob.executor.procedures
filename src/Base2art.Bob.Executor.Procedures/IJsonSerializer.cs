﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    
    public interface IJsonSerializer
    {
        string Serialize(object value);
        
        object Deserialize(string value, Type type);
    }
}
