﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    using System.Linq;
    
    public abstract class Procedure<TParams> : IProcedure<TParams>
        where TParams : IProcedureParameters
    {
        CleanLifecyclePhase IProcedure.SuggestedCleanLifecyclePhase
        {
            get { return TryGet(() => this.SuggestedCleanLifecyclePhase); }
        }

        DefaultLifecyclePhase IProcedure.SuggestedDefaultLifecyclePhase
        {
            get { return TryGet(() => this.SuggestedDefaultLifecyclePhase); }
        }

        SiteLifecyclePhase IProcedure.SuggestedSiteLifecyclePhase
        {
            get { return TryGet(() => this.SuggestedSiteLifecyclePhase); }
        }
        
        protected virtual CleanLifecyclePhase SuggestedCleanLifecyclePhase
        {
            get { return CleanLifecyclePhase.None; }
        }

        protected virtual DefaultLifecyclePhase SuggestedDefaultLifecyclePhase
        {
            get { return DefaultLifecyclePhase.None; }
        }

        protected virtual SiteLifecyclePhase SuggestedSiteLifecyclePhase
        {
            get { return SiteLifecyclePhase.None; }
        }

        public IProcedureExecutionResult Execute(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            TParams parameters,
            ProcedureLogger procedureLogger)
        {
            if (serviceLocator == null)
            {
                throw new ArgumentNullException("serviceLocator");
            }
            
            var exportedData = new VariableData();
            
            if (this.CanExecuteProcedure(serviceLocator, targetExecutionParameters, parameters, procedureLogger))
            {
                var result = this.ExecuteProcedure(
                    serviceLocator,
                    targetExecutionParameters,
                    (TParams)parameters,
                    procedureLogger,
                    exportedData)
                    ?? new ProcedureExecutionResult(false, this.ConvertVariables(exportedData));
                
                this.PostExecutionCleanUp(serviceLocator, targetExecutionParameters, parameters, procedureLogger, exportedData);
                
                return result;
            }
            
            return new ProcedureExecutionResult(null, this.ConvertVariables(exportedData));
        }

        IProcedureExecutionResult IProcedure.Execute(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            object parameters,
            ProcedureLogger procedureLogger)
        {
            return this.Execute(serviceLocator, targetExecutionParameters, (TParams)parameters, procedureLogger);
        }

        protected virtual bool CanExecuteProcedure(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            TParams parameters,
            ProcedureLogger procedureLogger)
        {
            return targetExecutionParameters.Status == TargetExecutionStatus.Success;
        }

        protected virtual void PostExecutionCleanUp(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            TParams parameters,
            ProcedureLogger procedureLogger,
            VariableData exportedData)
        {
        }
        
        protected abstract IProcedureExecutionResult ExecuteProcedure(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            TParams parameters,
            ProcedureLogger procedureLogger,
            VariableData exportedData);

        protected VariableDataTree ConvertVariables(VariableData exportedData, params IVariableDataTree[] childExportedData)
        {
            var tree = new VariableDataTree(DependencyIdentifierAttribute.Get(this));
            tree.Data = exportedData;
            tree.Children = (childExportedData ?? new IVariableDataTree[0]).Select(x => x.ToVariableTree()).ToArray();
            return tree;
        }
        
        private static T TryGet<T>(Func<T> suggestedLifecyclePhase)
        {
            try
            {
                return suggestedLifecyclePhase();
            }
            catch (Exception)
            {
                return default(T);
            }
        }
    }
}
