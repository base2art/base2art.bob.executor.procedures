﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    
    [Serializable]
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "SjY")]
    public class VariableData : Dictionary<string, object>, IVariableData
    {
        public VariableData()
        {
        }
        
        protected VariableData(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
        
        IEnumerable<string> IVariableData.Keys
        {
            get { return this.Keys; }
        }
        
        object IVariableData.this[string key]
        {
            get { return this[key]; }
        }

        public VariableData ToVariableData()
        {
            var data = new VariableData();
            foreach (var pair in this)
            {
                data.Add(pair.Key, pair.Value);
            }
            
            return data;
        }
        
        public bool Contains(string key)
        {
            return this.ContainsKey(key);
        }
    }
}
