﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;

    public interface IDependencyIdentifierTypeLocator
    {
        Type FindType(DependencyIdentifier dependencyIdentifier);
    }
}
