﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    using System.Linq;

    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
    public sealed class DependencyIdentifierAttribute : Attribute
    {
        public DependencyIdentifierAttribute(string organization, string product, string version)
        {
            if (string.IsNullOrWhiteSpace(organization))
            {
                throw new ArgumentNullException("organization");
            }
            
            if (string.IsNullOrWhiteSpace(product))
            {
                throw new ArgumentNullException("product");
            }
            
            Version parsedVersion;
            if (string.IsNullOrWhiteSpace(version) || !Version.TryParse(version, out parsedVersion))
            {
                throw new ArgumentNullException("version");
            }
            
            this.Organization = organization;
            this.Product = product;
            this.Version = parsedVersion;
        }
        
        public string Organization { get; internal set; }
        
        public string Product { get; internal set; }
        
        public Version Version { get; internal set; }
        
        public IDependencyIdentifier DependencyIdentifier
        {
            get
            {
                return new DependencyIdentifier
                {
                    Organization = this.Organization,
                    Product = this.Product,
                    Version = this.Version,
                };
            }
        }
        
        public static DependencyIdentifier Get(IProcedure procExecutable)
        {
            var type = procExecutable.GetType();
            var typeToInxpect = type;
            
            bool isOk = false;
            while (typeToInxpect != typeof(object))
            {
                if (typeToInxpect.IsGenericType && typeToInxpect.GetGenericTypeDefinition() == typeof(Procedure<>))
                {
                    isOk = true;
                    break;
                }
                
                typeToInxpect = typeToInxpect.BaseType;
            }
            
            if (!isOk)
            {
                return new DependencyIdentifier
                {
                    Organization = "Unknown",
                    Product = Guid.NewGuid().ToString("N"),
                    Version = new Version(0, 0, 0, 0)
                };
            }
            
            object[] attributes = type.GetCustomAttributes(false);
            var dependencyIdentifierAttribute = attributes.OfType<DependencyIdentifierAttribute>().FirstOrDefault();
            
            if (dependencyIdentifierAttribute == null)
            {
                throw new InvalidOperationException("The class you are using is not decorated correctly");
            }
            
            return dependencyIdentifierAttribute.DependencyIdentifier.ToDependencyIdentifier();
        }
    }
}
