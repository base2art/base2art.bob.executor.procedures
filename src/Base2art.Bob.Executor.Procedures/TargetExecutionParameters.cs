﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    
    public class TargetExecutionParameters
    {
        private readonly Guid id;
        
        private readonly string defaultWorkingDirectory;

        private readonly TargetExecutionStatus status;

        private readonly LifecyclePosition lifecyclePosition;

        private readonly string defaultArtifactDirectory;
        
        public TargetExecutionParameters(
            Guid id,
            string defaultWorkingDirectory,
            string defaultArtifactDirectory,
            TargetExecutionStatus status,
            LifecyclePosition lifecyclePosition)
        {
            this.id = id;
            this.defaultArtifactDirectory = defaultArtifactDirectory;
            this.lifecyclePosition = lifecyclePosition;
            this.defaultWorkingDirectory = defaultWorkingDirectory;
            this.status = status;
        }
        
        public virtual Guid Id
        {
            get { return this.id; }
        }
        
        public virtual string DefaultWorkingDirectory
        {
            get { return this.defaultWorkingDirectory; }
        }

        public string DefaultArtifactDirectory
        {
            get { return this.defaultArtifactDirectory; }
        }

        public virtual TargetExecutionStatus Status
        {
            get { return this.status; }
        }

        public ILifecyclePosition CurrentLifecyclePosition
        {
            get { return this.lifecyclePosition.ToLifecyclePosition(); }
        }
    }
}
