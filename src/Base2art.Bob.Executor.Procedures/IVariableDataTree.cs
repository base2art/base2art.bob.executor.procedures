﻿namespace Base2art.Bob.Executor.Procedures
{
    public interface IVariableDataTree
    {
        IDependencyIdentifier Procedure { get; }
        
        IVariableData Data { get; }

        IVariableDataTree[] Children { get; }

        VariableDataTree ToVariableTree();
    }
}
