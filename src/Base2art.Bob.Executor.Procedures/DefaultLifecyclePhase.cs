﻿namespace Base2art.Bob.Executor.Procedures
{
    public enum DefaultLifecyclePhase
    {
        None = 0,
        Validate = 1,
        Initialize = 2,
        GenerateSources = 3,
        ProcessSources = 4,
        GenerateResources = 5,
        ProcessResources = 6,
        Compile = 7,
        ProcessClasses = 8,
        GenerateTestSources = 9,
        ProcessTestSources = 10,
        GenerateTestResources = 11,
        ProcessTestResources = 12,
        TestCompile = 13,
        ProcessTestClasses = 14,
        PreTest = 15,
        Test = 16,
        PostTest = 17,
        PreparePackage = 18,
        Package = 19,
        PreIntegrationTest = 20,
        IntegrationTest = 21,
        PostIntegrationTest = 22,
        Verify = 23,
        Install = 24,
        Deploy = 25
    }
}
