﻿namespace Base2art.Bob.Executor.Procedures
{
    public interface IProcedure
    {
        CleanLifecyclePhase SuggestedCleanLifecyclePhase { get; }
        
        DefaultLifecyclePhase SuggestedDefaultLifecyclePhase { get; }
        
        SiteLifecyclePhase SuggestedSiteLifecyclePhase { get; }
        
        IProcedureExecutionResult Execute(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            object parameters,
            ProcedureLogger procedureLogger);
    }
    
    public interface IProcedure<TParams> : IProcedure
        where TParams : IProcedureParameters
    {
        IProcedureExecutionResult Execute(
            ProcedureServiceLocator serviceLocator,
            TargetExecutionParameters targetExecutionParameters,
            TParams parameters,
            ProcedureLogger procedureLogger);
    }
}