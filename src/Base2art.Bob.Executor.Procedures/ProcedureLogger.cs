﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    using System.ComponentModel;
    
    public class ProcedureLogger : IProcedureLogger
    {
        private readonly IProcedureLogger backingLogger;

        public ProcedureLogger(IProcedureLogger backingLogger)
        {
            if (backingLogger == null)
            {
                throw new ArgumentNullException("backingLogger");
            }
            
            this.backingLogger = backingLogger;
        }

        public Guid Id
        {
            get { return this.backingLogger.Id; }
        }
        
        public void WriteDebug(string content)
        {
            this.backingLogger.WriteDebug(content);
        }

        public void Write(string content)
        {
            this.backingLogger.Write(content);
        }

        public void WriteError(string content)
        {
            this.backingLogger.WriteError(content);
        }

        public void Append<T>(T dataObject)
        {
            this.backingLogger.Append(dataObject);
        }

        public void AppendError<T>(T dataObject)
        {
            this.backingLogger.AppendError(dataObject);
        }

        public void Flush(ProcedureExecutionResult result)
        {
            this.backingLogger.Flush(result);
        }
    }
}
