﻿namespace Base2art.Bob.Executor.Procedures
{
    public enum CleanLifecyclePhase
    {
        None = 0,
        PreClean = 1,
        Clean = 2,
        PostClean = 3
    }
}
