﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    
    public class DependencyIdentifier : IDependencyIdentifier
    {
        public string Organization { get; set; }

        public string Product { get; set; }

        public Version Version { get; set; }

        public static bool operator ==(DependencyIdentifier left, DependencyIdentifier right)
        {
            if (ReferenceEquals(left, right))
            {
                return true;
            }
            
            if (ReferenceEquals(left, null) || ReferenceEquals(right, null))
            {
                return false;
            }
            
            return left.Equals(right);
        }
        
        public static bool operator !=(DependencyIdentifier left, DependencyIdentifier right)
        {
            return !(left == right);
        }

        public static DependencyIdentifier Create(IDependencyIdentifier identifier)
        {
            if (identifier == null)
            {
                throw new ArgumentNullException("identifier");
            }
            
            return identifier.ToDependencyIdentifier();
        }
        
        public override bool Equals(object obj)
        {
            IDependencyIdentifier other = obj as IDependencyIdentifier;
            return this.AreEqual(other);
        }

        public DependencyIdentifier ToDependencyIdentifier()
        {
            return new DependencyIdentifier
            {
               Organization = this.Organization,
               Product = this.Product,
               Version = this.Version,
            };
        }
        
        public override int GetHashCode()
        {
            int hashCode = 0;
            unchecked
            {
                if (this.Organization != null)
                {
                    hashCode += 1000000007 * this.Organization.ToUpperInvariant().GetHashCode();
                }
                
                if (this.Product != null)
                {
                    hashCode += 1000000009 * this.Product.ToUpperInvariant().GetHashCode();
                }
                
                if (this.Version != null)
                {
                    hashCode += 1000000021 * Normalize(this.Version).GetHashCode();
                }
            }
            
            return hashCode;
        }

        bool IEquatable<IDependencyIdentifier>.Equals(IDependencyIdentifier other)
        {
            return this.AreEqual(other);
        }

        protected virtual bool AreEqual(IDependencyIdentifier other)
        {
            if (other == null)
            {
                return false;
            }
            
            var compareType = StringComparison.InvariantCultureIgnoreCase;
            
            var nonVersionsEqual = string.Equals(this.Organization, other.Organization, compareType)
                && string.Equals(this.Product, other.Product, compareType);
            
            if (!nonVersionsEqual)
            {
                return false;
            }
            
            if (ReferenceEquals(this.Version, other.Version))
            {
                return true;
            }
            
            if (ReferenceEquals(this.Version, null) || ReferenceEquals(null, other.Version))
            {
                return false;
            }
            
            return Normalize(this.Version) == Normalize(other.Version);
        }

        private static int Normalize(int part)
        {
            return part >= 0 ? part : 0;
        }

        private static Version Normalize(Version input)
        {
            return new Version(
                Normalize(input.Major),
                Normalize(input.Minor),
                Normalize(input.Build),
                Normalize(input.Revision));
        }
    }
}
