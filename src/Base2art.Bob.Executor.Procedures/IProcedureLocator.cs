﻿namespace Base2art.Bob.Executor.Procedures
{
    public interface IProcedureLocator
    {
        IProcedure GetProcedure(DependencyIdentifier procedure);
    }
}
