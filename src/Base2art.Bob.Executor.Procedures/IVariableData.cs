﻿namespace Base2art.Bob.Executor.Procedures
{
    using System.Collections.Generic;
    
    public interface IVariableData
    {
        int Count { get; }

        IEnumerable<string> Keys { get; }
        
        object this[string key] { get; }

        bool Contains(string key);
    }
}
