﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;

    public class ProcedureServiceLocator
    {
        private readonly IProcedureLocator procedureLocator;

        private readonly IProcedureLoggerFactory loggerFactory;

        private readonly IJsonSerializer serializer;

        private readonly IDependencyIdentifierTypeLocator procedureTypeFinder;
        
        public ProcedureServiceLocator(
            IProcedureLocator procedureLocator,
            IProcedureLoggerFactory loggerFactory,
            IJsonSerializer serializer,
            IDependencyIdentifierTypeLocator procedureTypeFinder)
        {
            if (loggerFactory == null)
            {
                throw new ArgumentNullException("loggerFactory");
            }
            
            if (procedureLocator == null)
            {
                throw new ArgumentNullException("procedureLocator");
            }
            
            if (serializer == null)
            {
                throw new ArgumentNullException("serializer");
            }
            
            if (procedureTypeFinder == null)
            {
                throw new ArgumentNullException("procedureTypeFinder");
            }
            
            this.loggerFactory = loggerFactory;
            this.procedureLocator = procedureLocator;
            this.procedureTypeFinder = procedureTypeFinder;
            this.serializer = serializer;
        }
        
        public IProcedureLocator ProcedureLocator
        {
            get { return this.procedureLocator; }
        }

        public IProcedureLoggerFactory LoggerFactory
        {
            get { return this.loggerFactory; }
        }

        public IJsonSerializer Serializer
        {
            get { return this.serializer; }
        }

        public IDependencyIdentifierTypeLocator DependencyIdentifierTypeLocator
        {
            get { return this.procedureTypeFinder; }
        }
    }
}
