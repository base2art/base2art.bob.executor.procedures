﻿namespace Base2art.Bob.Executor.Procedures
{
    public class ProcedureParameters : IProcedureParameters
    {
        public virtual string Description { get; set; }
    }
}
