﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;

    public interface IDependencyIdentifier : IEquatable<IDependencyIdentifier>
    {
        string Organization { get; }

        string Product { get; }

        Version Version { get; }

        DependencyIdentifier ToDependencyIdentifier();
    }
}
