﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class ProcessExecutionException : Exception, ISerializable
    {
        private readonly int exitCode;

        public ProcessExecutionException() : this("Exited with status Code: Unknown")
        {
            this.exitCode = -1;
        }

        public ProcessExecutionException(int exitCode) : base("Exited with status Code: " + exitCode)
        {
            this.exitCode = exitCode;
        }

        public ProcessExecutionException(int exitCode, string message) : base(message)
        {
            this.exitCode = exitCode;
        }

        public ProcessExecutionException(int exitCode, string message, Exception innerException) : base(message, innerException)
        {
            this.exitCode = exitCode;
        }

        public ProcessExecutionException(string message) : base(message)
        {
        }

        public ProcessExecutionException(string message, Exception innerException) : base(message, innerException)
        {
        }

        // This constructor is needed for serialization.
        protected ProcessExecutionException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            this.exitCode = info.GetInt32("ExitCode");
        }

        public int ExitCode
        {
            get { return this.exitCode; }
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("ExitCode", this.exitCode);
        }
    }
}
