﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    using System.Runtime.Serialization;
    
    [Serializable]
    public class ProcedureFailureException : Exception, ISerializable
    {
        public ProcedureFailureException()
        {
        }

        public ProcedureFailureException(string message)
            : base(message)
        {
        }

        public ProcedureFailureException(string message, Exception innerException) 
            : base(message, innerException)
        {
        }

        protected ProcedureFailureException(SerializationInfo info, StreamingContext context) 
            : base(info, context)
        {
        }
    }
}