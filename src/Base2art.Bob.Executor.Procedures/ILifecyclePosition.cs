﻿namespace Base2art.Bob.Executor.Procedures
{
    public interface ILifecyclePosition
    {
        SiteLifecyclePhase SiteLifecyclePhase { get; }

        DefaultLifecyclePhase DefaultLifecyclePhase { get; }

        CleanLifecyclePhase CleanLifecyclePhase { get; }

        bool IsEmpty();

        LifecyclePosition ToLifecyclePosition();
    }
}
