﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    using System.Linq;
    
    public class LifecyclePosition : ILifecyclePosition
    {
        private CleanLifecyclePhase cleanLifecyclePhase;
        
        private DefaultLifecyclePhase defaultLifecyclePhase;
        
        private SiteLifecyclePhase siteLifecyclePhase;
        
        public CleanLifecyclePhase CleanLifecyclePhase
        {
            get { return GetValidValue(this.cleanLifecyclePhase); }
            set { this.cleanLifecyclePhase = value; }
        }

        public DefaultLifecyclePhase DefaultLifecyclePhase
        {
            get { return GetValidValue(this.defaultLifecyclePhase); }
            set { this.defaultLifecyclePhase = value; }
        }

        public SiteLifecyclePhase SiteLifecyclePhase
        {
            get { return GetValidValue(this.siteLifecyclePhase); }
            set { this.siteLifecyclePhase = value; }
        }

        public bool IsEmpty()
        {
            return this.CleanLifecyclePhase == CleanLifecyclePhase.None
                && this.DefaultLifecyclePhase == DefaultLifecyclePhase.None
                && this.SiteLifecyclePhase == SiteLifecyclePhase.None;
        }

        public LifecyclePosition ToLifecyclePosition()
        {
            return (LifecyclePosition)this.MemberwiseClone();
        }

        private static bool IsValid<T>(T phase)
        {
            return ((T[])Enum.GetValues(typeof(T))).Contains(phase);
        }

        private static T GetValidValue<T>(T phase)
        {
            if (IsValid(phase))
            {
                return phase;
            }
            
            return default(T);
        }
    }
}
