﻿namespace Base2art.Bob.Executor.Procedures.CustomTypes
{
    using System;
    using System.ComponentModel;
    using System.Globalization;
    
    public class VersionTypeConverter : TypeConverterBase<string, Version>
    {
        protected override Version ConvertValueFrom(string inputValue, ITypeDescriptorContext context, CultureInfo culture)
        {
            return new Version(inputValue);
        }
    }
}
