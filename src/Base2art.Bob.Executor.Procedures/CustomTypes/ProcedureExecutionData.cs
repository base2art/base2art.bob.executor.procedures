﻿namespace Base2art.Bob.Executor.Procedures.CustomTypes
{
    public class ProcedureExecutionData
    {
        public DependencyIdentifier Procedure { get; set; }
        
        public DependencyIdentifier ProcedureParameters { get; set; }
        
        public object Parameters { get; set; }
    }
}
