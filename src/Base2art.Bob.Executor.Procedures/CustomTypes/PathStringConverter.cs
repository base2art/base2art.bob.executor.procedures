﻿namespace Base2art.Bob.Executor.Procedures.CustomTypes
{
    using System;
    using System.ComponentModel;
    using System.Globalization;

    public class PathStringTypeConverter : TypeConverterBase<string, PathString>
    {
        protected override PathString ConvertValueFrom(string inputValue, ITypeDescriptorContext context, CultureInfo culture)
        {
            return inputValue.Trim();
        }
    }
}
