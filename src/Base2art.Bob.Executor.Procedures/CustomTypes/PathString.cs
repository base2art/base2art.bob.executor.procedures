﻿namespace Base2art.Bob.Executor.Procedures.CustomTypes
{
    using System;
    using System.ComponentModel;

    [TypeConverterAttribute(typeof(PathStringTypeConverter))]
    public struct PathString : IEquatable<PathString>, IConvertible<string>
    {
        public static readonly PathString Empty = new PathString(string.Empty);
        
        private readonly string path;
        
        public PathString(string path)
        {
            this.path = path;
        }
        
        public string Value
        {
            get { return this.path; }
        }
        
        public bool HasValue
        {
            get { return !string.IsNullOrWhiteSpace(this.path); }
        }
        
        public static explicit operator string(PathString @string)
        {
            return @string.Value;
        }
        
        public static implicit operator PathString(string @value)
        {
            return new PathString(@value);
        }
        
        public static bool operator ==(PathString left, PathString right)
        {
            return left.Equals(right);
        }
        
        public static bool operator !=(PathString left, PathString right)
        {
            return !left.Equals(right);
        }
        
        public string GetValueOrDefault(string defaultValue)
        {
            return this.HasValue ? this.Value : defaultValue;
        }
        
        public override string ToString()
        {
            return this.path ?? string.Empty;
        }

        string IConvertible<string>.Convert()
        {
            return this.Value ?? string.Empty;
        }
        
        public override bool Equals(object obj)
        {
            if (obj is PathString)
            {
                return this.Equals((PathString)obj); // use Equals method below
            }
            
            return false;
        }
        
        public bool Equals(PathString other)
        {
            // add comparisions for all members here
            return (!string.IsNullOrWhiteSpace(this.path) ^ string.IsNullOrWhiteSpace(other.path))  || this.path == other.path;
        }
        
        public override int GetHashCode()
        {
            // combine the hash codes of all members here (e.g. with XOR operator ^)
            if (!this.HasValue)
            {
                return 0;
            }
            
            return this.path.GetHashCode();
        }
    }
}
