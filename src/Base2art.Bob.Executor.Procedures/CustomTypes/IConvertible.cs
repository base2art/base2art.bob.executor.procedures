﻿namespace Base2art.Bob.Executor.Procedures.CustomTypes
{
    public interface IConvertible<T>
    {
        T Convert();
    }
}
