﻿namespace Base2art.Bob.Executor.Procedures.CustomTypes
{
    using System;
    using System.ComponentModel;
    using System.Globalization;

    public abstract class TypeConverterBase<TInput, TOutput> : TypeConverter
    {
        public sealed override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(TInput) || sourceType == typeof(TOutput) || base.CanConvertFrom(context, sourceType);
        }
        
        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return destinationType == typeof(TOutput) || base.CanConvertTo(context, destinationType);
        }
        
        public sealed override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            if (value == null)
            {
                return this.CreateDefaultValue();
            }
            
            if (value is TOutput)
            {
                return value;
            }
            
            if (value is TInput)
            {
                var inputValue = (TInput)value;
                
                return this.ConvertValueFrom(inputValue, context, culture);
            }
            
            return base.ConvertFrom(context, culture, value);
        }

        protected virtual TOutput CreateDefaultValue()
        {
            return default(TOutput);
        }

        protected abstract TOutput ConvertValueFrom(TInput inputValue, ITypeDescriptorContext context, CultureInfo culture);
    }
}
