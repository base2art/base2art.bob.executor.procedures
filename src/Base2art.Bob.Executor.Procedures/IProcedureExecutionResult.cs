﻿namespace Base2art.Bob.Executor.Procedures
{
    public interface IProcedureExecutionResult
    {
        bool? IsSuccessfulRun { get; }
        
        IVariableDataTree ExportedData { get; }
    }
}
