﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class DependencyIdentifierFeature
    {
        [Test]
        public void ShouldVerifyEquality()
        {
            new DependencyIdentifier().Assert().Equality<IDependencyIdentifier>();
        }
        
        [Test]
        public void ShouldLoad()
        {
            Action mut = () => DependencyIdentifier.Create(null);
            mut.ShouldThrow<ArgumentNullException>();
        }
        
        [TestCase("Org", "Prod", "1.0.0.0", "Org", "Prod", "1.0.0.0", true)]
        [TestCase("Org", "Prod", "1.0.0.0", "org", "Prod", "1.0.0.0", true)]
        
        [TestCase("Org", "Prod", null, "Org", "Prod", "1.0.0.0", false)]
        [TestCase("Org", "Prod", "1.0.0.0", "org", "Prod", null, false)]
        
        [TestCase("Org", "Prod", "1.0.0.1", "org", "Prod", "1.0.0.0", false)]
        [TestCase("Org", "Prod", "1.0.0.0", "org", "Prod", "1.0.0.1", false)]
        
        [TestCase("Org", "Prod1", "1.0.0.0", "org", "Prod", "1.0.0.0", false)]
        [TestCase("Org", "Prod", "1.0.0.0", "org", "Prod1", "1.0.0.0", false)]
        
        [TestCase("Org1", "Prod", "1.0.0.0", "org", "Prod", "1.0.0.0", false)]
        [TestCase("Org", "Prod", "1.0.0.0", "org1", "Prod", "1.0.0.0", false)]
        public void ShouldVerifyEquality_Memberwise(
            string org, string prod, string version,
            string org1, string prod1, string version1,
            bool shouldEqual)
        {
            var dpi = new DependencyIdentifier
            {
                Organization = org,
                Product = prod,
                Version = version == null ? null : Version.Parse(version),
            };
            
            var dpi1 = new DependencyIdentifier
            {
                Organization = org1,
                Product = prod1,
                Version = version1 == null ? null :Version.Parse(version1),
            };
            
            (dpi.GetHashCode() == dpi1.GetHashCode()).Should().Be(shouldEqual);
            (dpi == dpi1).Should().Be(shouldEqual);
            
            var clone = dpi.ToDependencyIdentifier();
            
            (dpi.GetHashCode() == clone.GetHashCode()).Should().Be(true);
            (dpi == clone).Should().Be(true);
            
            (clone.GetHashCode() == dpi1.GetHashCode()).Should().Be(shouldEqual);
            (clone == dpi1).Should().Be(shouldEqual);
        }
    }
}


