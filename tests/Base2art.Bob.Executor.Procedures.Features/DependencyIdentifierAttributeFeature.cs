﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    using Base2art.Bob.Executor.Procedures.Basic;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;
    
    [TestFixture]
    public class DependencyIdentifierAttributeFeature
    {
        [Test]
        public void ShouldThrowException()
        {
            Action mut = () => DependencyIdentifierAttribute.Get(new BadTestProc());
            mut.ShouldThrow<InvalidOperationException>();
        }
        
        [Test]
        public void ShouldThrowException_Mock()
        {
            var dpi = DependencyIdentifierAttribute.Get(new Mock<IProcedure>().Object);
            dpi.Organization.Should().Be("Unknown");
            dpi.Product.Should().MatchRegex("[A-Za-z0-9]{32}");
            dpi.Version.Should().Be(new Version("0.0.0.0"));
        }
        
        [Test]
        public void ShouldThrowException_Good()
        {
            DependencyIdentifierAttribute.Get(new GoodTestProc()).Organization.Should().Be("Org");
        }
        
        [TestCase(null, "prod", "1.0.0.0")]
        [TestCase("org", null, "1.0.0.0")]
        [TestCase("org", "prod", null)]
        [TestCase("org", "prod", "a.b.c.d")]
        public void ShouldThrowExceptionWithBadCreate(string org, string prod, string version)
        {
            Action attr = () => new DependencyIdentifierAttribute(org, prod, version);
            attr.ShouldThrow<ArgumentNullException>();
        }
        
        [TestCase("org", "prod", "1.0.0.0")]
        public void ShouldCreate(string org, string prod, string version)
        {
            var id = new DependencyIdentifierAttribute(org, prod, version);
            id.Organization.Should().Be(org);
            id.Product.Should().Be(prod);
            id.Version.Should().Be(Version.Parse(version));
            
            var dpi = new DependencyIdentifier
            {
                Organization = org,
                Product = prod,
                Version = Version.Parse(version)
            };
            
            id.DependencyIdentifier.Should().Be(dpi);
        }
        
        [DependencyIdentifier("Org", "Prod", "1.0.0.0")]
        private class GoodTestProc : Procedure<NullProcedureParameters>
        {
            protected override IProcedureExecutionResult ExecuteProcedure(
                ProcedureServiceLocator serviceLocator,
                TargetExecutionParameters targetExecutionParameters,
                NullProcedureParameters parameters,
                ProcedureLogger procedureLogger,
                VariableData exportedData)
            {
                exportedData.Add("Name", "Value");
                return new ProcedureExecutionResult(true, this.ConvertVariables(exportedData));
            }
        }
        
        [DependencyIdentifier("Org", "Prod-Params", "1.0.0.0")]
        private class GoodTestProcParams : ProcedureParameters
        {
        }
        
        private class BadTestProc : Procedure<NullProcedureParameters>
        {
            protected override IProcedureExecutionResult ExecuteProcedure(
                ProcedureServiceLocator serviceLocator,
                TargetExecutionParameters targetExecutionParameters,
                NullProcedureParameters parameters,
                ProcedureLogger procedureLogger,
                VariableData exportedData)
            {
                exportedData.Add("Name", "Value");
                return new ProcedureExecutionResult(true, this.ConvertVariables(exportedData));
            }
        }
    }
}
