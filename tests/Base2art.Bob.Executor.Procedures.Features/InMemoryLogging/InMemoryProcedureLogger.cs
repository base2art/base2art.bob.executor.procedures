﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;
    
    [ExcludeFromCodeCoverage]
    public class InMemoryProcedureLogger : Component, IProcedureLogger
    {
        private readonly List<LogItem> debug = new List<LogItem>();

        private readonly List<LogItem> error = new List<LogItem>();

        private readonly List<LogItem> output = new List<LogItem>();
        
        private readonly List<InMemoryProcedureLogger> childLoggers = new List<InMemoryProcedureLogger>();

        private readonly Guid loggerId;
        
        public InMemoryProcedureLogger()
        {
            this.loggerId = Guid.NewGuid();
        }
        
        /// <summary>
        /// Gets the id of the logger.
        /// </summary>
        public Guid Id
        {
            get { return this.loggerId; }
        }
        
        public LogItem[] Error
        {
            get { return this.error.ToArray(); }
        }

        public LogItem[] Output
        {
            get { return this.output.ToArray(); }
        }

        public LogItem[] Debug
        {
            get { return this.debug.ToArray(); }
        }

        public IList<InMemoryProcedureLogger> ChildLoggers
        {
            get { return this.childLoggers; }
        }
        
        public void WriteDebug(string content)
        {
            this.debug.Add(this.Create(content));
        }

        public void Write(string content)
        {
            this.output.Add(this.Create(content));
        }

        public void WriteError(string content)
        {
            this.error.Add(this.Create(content));
        }

        public void Append<T>(T dataObject)
        {
            this.output.Add(this.Create(dataObject));
        }

        public void AppendError<T>(T dataObject)
        {
            this.error.Add(this.Create(dataObject));
        }

        public void Flush(ProcedureExecutionResult result)
        {
		}
        
        public IProcedureLogger CreateSubLoggerFor(
            TargetExecutionParameters targetExecutionParameters,
            Guid parentLoggerId,
            DependencyIdentifier procedure,
            DependencyIdentifier procedureParameters)
        {
            var childLogger = new InMemoryProcedureLogger();
            this.childLoggers.Add(childLogger);
            return childLogger;
        }
        
        private LogItem Create(object content)
        {
            return new LogItem
            {
                Date = DateTimeOffset.Now,
                Id = Guid.NewGuid(),
                Value = content
            };
        }
    }
}
