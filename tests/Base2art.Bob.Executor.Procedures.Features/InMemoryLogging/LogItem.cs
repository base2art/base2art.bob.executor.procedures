﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    
    public class LogItem
    {
        public Guid Id { get; set; }
        public DateTimeOffset Date { get; set; }
        public object Value { get; set; }
    }
}