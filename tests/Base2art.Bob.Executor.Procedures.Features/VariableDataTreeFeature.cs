﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    using Base2art.Bob.Executor.Procedures.Basic;
    using Base2art.Bob.Executor.Procedures.CustomTypes;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;
    
    [TestFixture]
    public class VariableDataTreeFeature
    {
        [Test]
        public void TestCtor()
        {
            Action mut = () => new VariableDataTree(null);
            mut.ShouldThrow<ArgumentNullException>();
        }
        
        [Test]
        public void TestAttrs()
        {
            var tree = new VariableDataTree(new DependencyIdentifier());
            
            var variableData = new VariableData();
            variableData.Add("a", "b");
            var content = Newtonsoft.Json.JsonConvert.SerializeObject(variableData, Newtonsoft.Json.Formatting.Indented);
            Console.WriteLine(content);
            
            
            var obj = Newtonsoft.Json.JsonConvert.DeserializeObject<VariableData>(content);
            tree.Assert().NeverNull(x => x.Children);
            tree.Assert().NeverNull(x => x.Data);
            obj["a"].Should().Be("b");
        }
        
        
        
        [Test]
        public void ShouldExecuteTree()
        {
            var targetParameters = new TargetExecutionParameters(
                Guid.Empty,
                "c:\\Temp\\",
                "c:\\Temp\\Artifacts",
                TargetExecutionStatus.Success,
                new LifecyclePosition());
            var logger = new InMemoryProcedureLogger();
            
            var proc = new SequenceProcedure();
            var parms = new SequenceProcedureParameters();
            parms.Procedures = new Base2art.Bob.Executor.Procedures.CustomTypes.ProcedureExecutionData[]
            {
                new ProcedureExecutionData
                {
                    ProcedureParameters = new DependencyIdentifier{ Organization = "Base2art", Product = "Echo-Parameters", Version = new Version("1.0.0.0") },
                    Procedure = new DependencyIdentifier{ Organization = "Base2art", Product = "Echo", Version = new Version("1.0.0.0") },
                    Parameters = new EchoProcedureParameters {Message = "First Task"}
                },
                new ProcedureExecutionData
                {
                    ProcedureParameters = new DependencyIdentifier{ Organization = "Base2art", Product = "Echo-Parameters", Version = new Version("1.0.0.0") },
                    Procedure = new DependencyIdentifier{ Organization = "Base2art", Product = "Echo", Version = new Version("1.0.0.0") },
                    Parameters = new EchoProcedureParameters{Message = "Second Task"}
                },
                new ProcedureExecutionData
                {
                    ProcedureParameters = new DependencyIdentifier{ Organization = "Base2art", Product = "Null-Parameters", Version = new Version("1.0.0.0") },
                    Procedure = new DependencyIdentifier{ Organization = "Base2art", Product = "Export", Version = new Version("1.0.0.0") },
                    Parameters = new NullProcedureParameters()
                },
            };
            
            var result = proc.Execute(this.MakeLocator(logger), targetParameters, parms, new ProcedureLogger(logger));
            
            result.ExportedData.Data.Count.Should().Be(0);
            
            result.ExportedData.Children[2].Data.Count.Should().Be(1);
            result.ExportedData.Children[2].Procedure.Should()
                .Be( new DependencyIdentifier{ Organization = "Base2art", Product = "Export", Version = new Version("1.0.0.0") });
            
            result.ExportedData.Children[2].Data.Contains("Name");
            result.ExportedData.Children[2].Data["Name"].Should().Be("Value");
            result.ExportedData.Children[2].Data.Keys.Should().BeEquivalentTo("Name");
        }
        
        
        private ProcedureServiceLocator MakeLocator(InMemoryProcedureLogger logger)
        {
            var subLogger = new Mock<IProcedureLoggerFactory>(MockBehavior.Strict);
            var @object = subLogger.Object;
            var procedureLogger = new Mock<IProcedureLocator>(MockBehavior.Strict);
            subLogger.Setup(x => x.CreateLoggerFor(It.IsAny<TargetExecutionParameters>(), It.IsAny<Guid>(), It.IsAny<DependencyIdentifier>(), It.IsAny<DependencyIdentifier>()))
                .Returns<TargetExecutionParameters, Guid, DependencyIdentifier, DependencyIdentifier>(logger.CreateSubLoggerFor);
            
            procedureLogger.Setup(x => x.GetProcedure(new DependencyIdentifier{ Organization = "Base2art", Product = "Echo", Version = new Version(1,0,0,0)}))
                .Returns(() => new EchoProcedure());
            
            procedureLogger.Setup(x => x.GetProcedure(new DependencyIdentifier{ Organization = "Base2art", Product = "Export", Version = new Version(1,0,0,0)}))
                .Returns(() => new TestProc());
            
            
            var typeLocator = new Mock<IDependencyIdentifierTypeLocator>();
            
            typeLocator.Setup(x => x.FindType(It.IsAny<DependencyIdentifier>()))
                .Returns(typeof(object));
            
            var current = new object();
            var mock = new Mock<IJsonSerializer>();
            
            mock.Setup(x => x.Serialize(It.IsAny<object>()))
                .Returns("ABC")
                .Callback<object>(x => current = x);
            
            mock.Setup(x => x.Deserialize(It.IsAny<string>(), It.IsAny<Type>()))
                .Returns(() => current);
            
            return new ProcedureServiceLocator(
                procedureLogger.Object,
                @object,
                mock.Object,
                typeLocator.Object);
        }
        
        [DependencyIdentifier("Base2art", "Export", "1.0.0.0")]
        private class TestProc : Procedure<NullProcedureParameters>
        {
            protected override IProcedureExecutionResult ExecuteProcedure(
                ProcedureServiceLocator serviceLocator,
                TargetExecutionParameters targetExecutionParameters,
                NullProcedureParameters parameters,
                ProcedureLogger procedureLogger,
                VariableData exportedData)
            {
                exportedData.Add("Name", "Value");
                return new ProcedureExecutionResult(true, this.ConvertVariables(exportedData));
            }
        }
    }
}
