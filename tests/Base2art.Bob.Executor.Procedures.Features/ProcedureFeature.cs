﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class ProcedureFeature
    {
        [Test]
        public void ShouldGetPhases()
        {
            IProcedure proc = new FakeProcedure();
            proc.SuggestedCleanLifecyclePhase.Should().Be(CleanLifecyclePhase.Clean);
            proc.SuggestedDefaultLifecyclePhase.Should().Be(DefaultLifecyclePhase.Install);
            proc.SuggestedSiteLifecyclePhase.Should().Be(SiteLifecyclePhase.None);
        }
        
        [Test]
        public void ShouldExecute_AlwaysHasReturn_Coalescing()
        {
            FakeProcedure proc = new FakeProcedure();
            proc.ProcedureExecutionResult = null;
            var result = proc.Execute(this.MakeLocator(), MakeTargetParams(), null, null);
            
            result.Should().NotBeNull();
            result.IsSuccessfulRun.Should().BeFalse();
        }
        
        [Test]
        public void ShouldExecute_AlwaysHasReturn_NoCoalescing()
        {
            FakeProcedure proc = new FakeProcedure();
            proc.ProcedureExecutionResult = new ProcedureExecutionResult(true, new VariableDataTree(new DependencyIdentifier()));
            
            var result = proc.Execute(this.MakeLocator(), this.MakeTargetParams(), null, null);
            
            result.Should().NotBeNull();
            result.IsSuccessfulRun.Should().BeTrue();
        }
        
        [Test]
        public void ShouldExecute_AlwaysHasReturn_Coalescing_NonGeneric()
        {
            var proc = new FakeProcedure();
            proc.ProcedureExecutionResult = null;
            var result = proc.As<IProcedure>().Execute(this.MakeLocator(), this.MakeTargetParams(), null, null);
            
            result.Should().NotBeNull();
            result.IsSuccessfulRun.Should().BeFalse();
        }
        
        [Test]
        public void ShouldExecute_AlwaysHasReturn_NoCoalescing_NonGeneric()
        {
            FakeProcedure proc = new FakeProcedure();
            proc.ProcedureExecutionResult = new ProcedureExecutionResult(true, new VariableDataTree(new DependencyIdentifier()));
            
            var result = proc.As<IProcedure>().Execute(this.MakeLocator(), this.MakeTargetParams(), null, null);
            
            result.Should().NotBeNull();
            result.IsSuccessfulRun.Should().BeTrue();
        }
        
        [TestCase(true, true, true)]
        [TestCase(true, false, false)]
        [TestCase(false, true, null)]
        [TestCase(false, false, null)]
        public void CanExecuteRespectsOverride(bool executionPlan, bool resultSuccess, bool? expectedOutput)
        {
            FakeProcedure proc = new FakeProcedure();
            proc.CanExecute = executionPlan;
            proc.ProcedureExecutionResult = new ProcedureExecutionResult(resultSuccess, new VariableDataTree(new DependencyIdentifier()));
            
            var result = proc.As<IProcedure>().Execute(this.MakeLocator(), this.MakeTargetParams(), null, null);
            
            result.Should().NotBeNull();
            result.IsSuccessfulRun.Should().Be(expectedOutput);
        }
        
        [DependencyIdentifier("ORG", "PROD", "1.0.0.0")]
        private class FakeProcedure : Procedure<FakeProcedureParameters>
        {
            public ProcedureExecutionResult ProcedureExecutionResult { get; set; }
            public bool? CanExecute { get; set; }
            
            protected override bool CanExecuteProcedure(
                ProcedureServiceLocator serviceLocator,
                TargetExecutionParameters targetExecutionParameters,
                FakeProcedureParameters parameters,
                ProcedureLogger procedureLogger)
            {
                if (this.CanExecute.HasValue)
                {
                    return this.CanExecute.Value;
                }
                
                return base.CanExecuteProcedure(serviceLocator, targetExecutionParameters, parameters, procedureLogger);
            }
            
            protected override IProcedureExecutionResult ExecuteProcedure(
                ProcedureServiceLocator serviceLocator,
                TargetExecutionParameters targetExecutionParameters,
                FakeProcedureParameters parameters,
                ProcedureLogger procedureLogger,
                VariableData exportedData)
            {
                return this.ProcedureExecutionResult;
            }
            
            protected override CleanLifecyclePhase SuggestedCleanLifecyclePhase
            {
                get { return CleanLifecyclePhase.Clean; }
            }
            
            protected override DefaultLifecyclePhase SuggestedDefaultLifecyclePhase
            {
                get { return DefaultLifecyclePhase.Install; }
            }
        }
        
        private class FakeProcedureParameters : ProcedureParameters
        {
        }

        TargetExecutionParameters MakeTargetParams()
        {
            return new TargetExecutionParameters(
                Guid.Empty,
                "c:\\temp\\",
                "c:\\Temp\\Artifacts",
                TargetExecutionStatus.Success,
                new LifecyclePosition());
        }

        private ProcedureServiceLocator MakeLocator()
        {
            return new ProcedureServiceLocator(
                new Mock<IProcedureLocator>().Object,
                new Mock<IProcedureLoggerFactory>().Object,
                new Mock<IJsonSerializer>().Object,
                new Mock<IDependencyIdentifierTypeLocator>().Object);
        }
    }
}


