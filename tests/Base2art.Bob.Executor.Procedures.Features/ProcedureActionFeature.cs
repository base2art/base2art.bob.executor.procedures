﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class ProcedureActionFeature
    {
        [Test]
        public void ShouldExecute_AlwaysHasReturn_Coalescing()
        {
            FakeProcedure proc = new FakeProcedure();
            proc.ProcedureExecutionResult = null;
            var result = proc.Execute(this.MakeLocator(), this.MakeTargetParams(), null, null);
            result.Should().NotBeNull();
            result.IsSuccessfulRun.Should().BeTrue();
        }

        [Test]
        public void ShouldExecute_AlwaysHasReturn_NoCoalescing()
        {
            FakeProcedure proc = new FakeProcedure();
            proc.ProcedureExecutionResult = new ProcedureExecutionResult(true, new VariableDataTree(new DependencyIdentifier()));
            var result = proc.Execute(this.MakeLocator(), this.MakeTargetParams(), null, null);
            result.Should().NotBeNull();
            result.IsSuccessfulRun.Should().BeTrue();
        }

        [Test]
        public void ShouldExecute_AlwaysHasReturn_Coalescing_NonGeneric()
        {
            var proc = new FakeProcedure();
            proc.ProcedureExecutionResult = null;
            var result = proc.As<IProcedure>().Execute(this.MakeLocator(), this.MakeTargetParams(), null, null);
            result.Should().NotBeNull();
            result.IsSuccessfulRun.Should().BeTrue();
        }

        [Test]
        public void ShouldExecute_AlwaysHasReturn_NoCoalescing_NonGeneric()
        {
            FakeProcedure proc = new FakeProcedure();
            proc.ProcedureExecutionResult = new ProcedureExecutionResult(true, new VariableDataTree(new DependencyIdentifier()));
            var result = proc.As<IProcedure>().Execute(this.MakeLocator(), this.MakeTargetParams(), null, null);
            result.Should().NotBeNull();
            result.IsSuccessfulRun.Should().BeTrue();
        }

        [DependencyIdentifier("ORG", "PROD", "1.0.0.0")]
        private class FakeProcedure : ProcedureAction<FakeProcedureParameters>
        {
            public ProcedureExecutionResult ProcedureExecutionResult { get; set; }

            protected override void ExecuteProcedureAction(
                ProcedureServiceLocator serviceLocator,
                TargetExecutionParameters targetExecutionParameters,
                FakeProcedureParameters parameters,
                ProcedureLogger procedureLogger,
                VariableData exportedData)
            {
                if (this.ProcedureExecutionResult == null)
                {
                    return;
                }
                
                if (this.ProcedureExecutionResult.IsSuccessfulRun.GetValueOrDefault())
                {
                    return;
                }
                
                if (!this.ProcedureExecutionResult.IsSuccessfulRun.GetValueOrDefault())
                {
                    throw new ProcedureFailureException();
                }
            }
        }

        private class FakeProcedureParameters : ProcedureParameters
        {
        }

        TargetExecutionParameters MakeTargetParams()
        {
            return new TargetExecutionParameters(
                Guid.Empty,
                "c:\\temp\\",
                "c:\\Temp\\Artifacts",
                TargetExecutionStatus.Success,
                new LifecyclePosition());
        }

        private ProcedureServiceLocator MakeLocator()
        {
            return new ProcedureServiceLocator(
                new Mock<IProcedureLocator>().Object,
                new Mock<IProcedureLoggerFactory>().Object,
                new Mock<IJsonSerializer>().Object,
                new Mock<IDependencyIdentifierTypeLocator>().Object);
        }
    }
}




