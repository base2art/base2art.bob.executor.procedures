﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    using Base2art;
    using FluentAssertions;
    using NUnit.Framework;
    
    [TestFixture]
    public class ProcedureFailureExceptionFeature
    {
        [Test]
        public void ShouldSerializeAndDeserializeException()
        {
            Insist.ThatException<ProcedureFailureException>().IsImplementedCorrectly();
        }
        
        [Test]
        public void ShouldSerializeAndDeserializeExceptionWithExitCode()
        {
            Insist.ThatException<ProcessExecutionException>()
                .IsImplementedCorrectly(() => new ProcessExecutionException(99, "Message"))
                .ExitCode.Should().Be(99);
            
            Insist.ThatException<ProcessExecutionException>()
                .IsImplementedCorrectly(() => new ProcessExecutionException(99, "Message", new InvalidOperationException("Op Invalid")))
                .ExitCode.Should().Be(99);
            
            Insist.ThatException<ProcessExecutionException>()
                .IsImplementedCorrectly(() => new ProcessExecutionException(99))
                .ExitCode.Should().Be(99);
        }
    }
}
