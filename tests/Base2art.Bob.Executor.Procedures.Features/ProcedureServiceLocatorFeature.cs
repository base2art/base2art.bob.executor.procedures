﻿namespace Base2art.Bob.Executor.Procedures
{
    using NUnit.Framework;
    
    [TestFixture]
    public class ProcedureServiceLocatorFeature
    {
        [Test]
        public void ShouldLoad()
        {
            Insist.ThatConstructor<ProcedureServiceLocator>().ValidatesNullArguments();
        }
    }
}
