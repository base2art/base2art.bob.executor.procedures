﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    using Base2art;
    using NUnit.Framework;

    [TestFixture]
    public class ProcessExecutionExceptionFeature
    {
        [Test]
        public void ShouldSerializeAndDeserializeException()
        {
            Insist.ThatException<ProcessExecutionException>().IsImplementedCorrectly();
        }
    }
}


