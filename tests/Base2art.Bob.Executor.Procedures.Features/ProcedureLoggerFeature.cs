﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    using System.Linq.Expressions;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;
    
    [TestFixture]
    public class ProcedureLoggerFeature
    {
        private Mock<IProcedureLogger> mock;

        [SetUp]
        public void BeforeEach()
        {
            this.mock = new Mock<IProcedureLogger>(MockBehavior.Strict);
        }

        [TearDown]
        public void AfterEach()
        {
            this.mock.VerifyAll();
        }

        [Test]
        public void ShouldProxy_ThrowException()
        {
            Action mut = () => new ProcedureLogger(null);
            mut.ShouldThrow<ArgumentNullException>();
        }

        [Test]
        public void ShouldProxy_WriteDebug()
        {
            this.Run(x => x.WriteDebug("input"));
        }

        [Test]
        public void ShouldProxy_Write()
        {
            this.Run(x => x.Write("input"));
        }

        [Test]
        public void ShouldProxy_WriteError()
        {
            this.Run(x => x.WriteError("input"));
        }

        [Test]
        public void ShouldProxy_Append()
        {
            this.Run(x => x.Append("input"));
        }

        [Test]
        public void ShouldProxy_AppendError()
        {
            this.Run(x => x.AppendError("input"));
        }

        [Test]
        public void ShouldProxy_Flush()
        {
            this.Run(x => x.Flush(It.IsAny<ProcedureExecutionResult>()));
        }

        [Test]
        public void ShouldGetId()
        {
            var id = Guid.NewGuid();
            this.mock.Setup(x=>x.Id).Returns(id);
            IProcedureLogger logger = new ProcedureLogger(this.mock.Object);
            logger.Id.Should().Be(id);
        }
        
        private void Run(Expression<Action<IProcedureLogger>> action)
        {
            this.mock.Setup(action);
            IProcedureLogger logger = new ProcedureLogger(this.mock.Object);
            action.Compile()(logger);
        }
    }
}
