﻿namespace Base2art.Bob.Executor.Procedures
{
    public enum ErrorType
    {
        NotImplemented,
        ProcedureFailureException,
        ProcessExecutionException,
        AggregateException,
        SeeException
    }
}


