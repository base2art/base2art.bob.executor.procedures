﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    using System.Threading.Tasks;
    using Base2art.Bob.Executor.Procedures.CustomTypes;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    public class ProcedureExecutionRunnerFeature
    {
        LifecyclePosition lifecyclePosition = new LifecyclePosition { CleanLifecyclePhase = CleanLifecyclePhase.PostClean };
        
        [TestCase(ErrorType.NotImplemented, false)]
        [TestCase(ErrorType.AggregateException, false)]
        [TestCase(ErrorType.ProcedureFailureException, false)]
        [TestCase(ErrorType.ProcessExecutionException, false)]
        [TestCase(null, true)]
        public void Should(ErrorType? errorType, bool isSuccessful)
        {
            Mock<IProcedureLocator> locator = new Mock<IProcedureLocator>();
            Mock<IProcedureLoggerFactory> loggerFactory = new Mock<IProcedureLoggerFactory>();
            Mock<IProcedureLogger> logger = new Mock<IProcedureLogger>();
            
            var item  = ProcedureExecutionRunner.ExecuteProcedure(
                new ProcedureServiceLocator(locator.Object, loggerFactory.Object, new Mock<IJsonSerializer>().Object, new Mock<IDependencyIdentifierTypeLocator>().Object),
                new TargetExecutionParameters(
                    Guid.Empty,
                    "c:\\Temp\\working\\",
                    "c:\\Temp\\artifacts\\",
                    TargetExecutionStatus.Success,
                    lifecyclePosition),
                new TestProc(),
                new TestProcParams { ErrorType = errorType },
                logger.Object);
            
            item.IsSuccessfulRun.Should().Be(isSuccessful);
        }
        
        [Test]
        public void Should_VerifyNulls()
        {
            var parms = new TargetExecutionParameters(
                Guid.Empty,
                "c:\\Temp\\working\\",
                "c:\\Temp\\artifacts\\",
                TargetExecutionStatus.Success,
                lifecyclePosition);
            Mock<IProcedureLocator> locator = new Mock<IProcedureLocator>();
            Mock<IProcedureLoggerFactory> loggerFactory = new Mock<IProcedureLoggerFactory>();
            
            var procedureServiceLocator = new ProcedureServiceLocator(locator.Object, loggerFactory.Object, new Mock<IJsonSerializer>().Object, new Mock<IDependencyIdentifierTypeLocator>().Object);
            Action mut  = () => ProcedureExecutionRunner.ExecuteProcedure(null, procedureServiceLocator, parms, null);
            mut.ShouldThrow<ProcedureFailureException>();
        }
        
        [Test]
        public void Should_VerifyNulls_ProcedureExecutionResult()
        {
            Insist.ThatConstructor<ProcedureExecutionResult>().ValidatesNullArguments();
        }
        
        [Test]
        public void Should_DefaultValues_Phases()
        {
            IProcedure item = new TestProc();
            item.SuggestedCleanLifecyclePhase.Should().Be(CleanLifecyclePhase.None);
            item.SuggestedSiteLifecyclePhase.Should().Be(SiteLifecyclePhase.None);
            item.SuggestedDefaultLifecyclePhase.Should().Be(DefaultLifecyclePhase.None);
        }
        
        [Test]
        public void Should_Verify_HasData()
        {
            var parms = new TargetExecutionParameters(
                Guid.Empty,
                "c:\\Temp\\working\\",
                "c:\\Temp\\artifacts\\",
                TargetExecutionStatus.Success,
                lifecyclePosition);
            Mock<IProcedureLocator> locator = new Mock<IProcedureLocator>();
            Mock<IProcedureLoggerFactory> loggerFactory = new Mock<IProcedureLoggerFactory>();
            Mock<IProcedureLogger> logger = new Mock<IProcedureLogger>();
            
            loggerFactory.Setup(x => x.CreateLoggerFor(It.IsAny<TargetExecutionParameters>(), It.IsAny<DependencyIdentifier>(), It.IsAny<DependencyIdentifier>()))
                .Returns(logger.Object);
            
            var ped = new ProcedureExecutionData();
            
            ped.Parameters = new {};
            ped.Procedure = new DependencyIdentifier();
            ped.ProcedureParameters = new DependencyIdentifier();
            
            var procedureServiceLocator = new ProcedureServiceLocator(locator.Object, loggerFactory.Object, new Mock<IJsonSerializer>().Object, new Mock<IDependencyIdentifierTypeLocator>().Object);
            Action mut  = () => ProcedureExecutionRunner.ExecuteProcedure(ped, procedureServiceLocator, parms, null);
            mut.ShouldThrow<ProcedureFailureException>().And.Message.Contains("Unable to find the type with id");
        }
        
        [Test]
        public void Should_Verify_NoProc()
        {
            var parms = new TargetExecutionParameters(
                Guid.Empty,
                "c:\\Temp\\working\\",
                "c:\\Temp\\artifacts\\",
                TargetExecutionStatus.Success,
                lifecyclePosition);
            Mock<IProcedureLocator> locator = new Mock<IProcedureLocator>();
            Mock<IProcedureLoggerFactory> loggerFactory = new Mock<IProcedureLoggerFactory>();
            
            var ped = new ProcedureExecutionData();
            
            ped.Parameters = new {};
            ped.Procedure = null;
            ped.ProcedureParameters = new DependencyIdentifier();
            
            var procedureServiceLocator = new ProcedureServiceLocator(locator.Object, loggerFactory.Object, new Mock<IJsonSerializer>().Object, new Mock<IDependencyIdentifierTypeLocator>().Object);
            Action mut  = () => ProcedureExecutionRunner.ExecuteProcedure(ped, procedureServiceLocator, parms, null);
            mut.ShouldThrow<ProcedureFailureException>().And.Message.Should().Be("Prodecure is null.");
        }
        
        [Test]
        public void Should_Verify_NoParams()
        {
            var parms = new TargetExecutionParameters(
                Guid.Empty,
                "c:\\Temp\\working\\",
                "c:\\Temp\\artifacts\\",
                TargetExecutionStatus.Success,
                lifecyclePosition);
            Mock<IProcedureLocator> locator = new Mock<IProcedureLocator>();
            Mock<IProcedureLoggerFactory> loggerFactory = new Mock<IProcedureLoggerFactory>();
            
            var ped = new ProcedureExecutionData();
            
            ped.Parameters = new {};
            ped.Procedure = new DependencyIdentifier();
            ped.ProcedureParameters = null;
            
            var procedureServiceLocator = new ProcedureServiceLocator(locator.Object, loggerFactory.Object, new Mock<IJsonSerializer>().Object, new Mock<IDependencyIdentifierTypeLocator>().Object);
            Action mut  = () => ProcedureExecutionRunner.ExecuteProcedure(ped, procedureServiceLocator, parms, null);
            mut.ShouldThrow<ProcedureFailureException>().And.Message.Should().Be("ProcedureParameters is null.");
        }

        [TestCase(typeof(Exception), false)]
        [TestCase(typeof(ProcessExecutionException), false)]
        [TestCase(typeof(ProcedureFailureException), false)]
        public void ShouldExecuteProcWithErrorHandling(Type exceptionType, bool expectedSuccess)
        {
            var proc = new Mock<IProcedure>();
            var locator = new Mock<IProcedureLocator>();
            var loggerFactory = new Mock<IProcedureLoggerFactory>();
            var loggerItem = new Mock<IProcedureLogger>();
            
            var targetExecutionParameters = new TargetExecutionParameters(Guid.NewGuid(), "c:\\Temp\\", "c:\\Temp\\artifacts\\", TargetExecutionStatus.Success, new Procedures.LifecyclePosition());
            var anonymousType = new { };
            var procedureLogger = loggerItem.Object;
//            
//            proc.Setup(x => x.Execute(It.IsAny<ProcedureServiceLocator>(), targetExecutionParameters, anonymousType, It.IsAny<ProcedureLogger>()))
//                .Throws((Exception)Activator.CreateInstance(exceptionType));
            
            //            this.loggerFactory.Setup(x => x.CreateSubLoggerFactory()).Returns(this.subLoggerFactory.Object);
            //            this.loggerFactory.Setup(x => x.EnsureEndedTime(this.loggerId));
            
            var parms = new TestProcParams{
                ErrorType = ErrorType.SeeException,
                ErrorTypeToThrow = exceptionType,
            };
            
            
            var procedureServiceLocator = new ProcedureServiceLocator(locator.Object, loggerFactory.Object, new Mock<IJsonSerializer>().Object, new Mock<IDependencyIdentifierTypeLocator>().Object);
            
            var result = ProcedureExecutionRunner.ExecuteProcedure(
                procedureServiceLocator, targetExecutionParameters, new TestProc(), parms, procedureLogger);
            
            //            var result = this.Runner().ExecuteProcedure(proc.Object, targetExecutionParameters, anonymousType, procedureLogger);
            result.IsSuccessfulRun.Should().Be(expectedSuccess);
        }
        
        [DependencyIdentifier("ORG", "PROD", "1.0.0.0")]
        private class TestProc : ProcedureAction<TestProcParams>
        {
            protected override void ExecuteProcedureAction(ProcedureServiceLocator serviceLocator, TargetExecutionParameters targetExecutionParameters, TestProcParams parameters, ProcedureLogger procedureLogger, VariableData exportedData)
            {
                if (parameters.ErrorType == ErrorType.SeeException)
                {
                    throw (Exception)Activator.CreateInstance(parameters.ErrorTypeToThrow);
                }
                
                if (parameters.ErrorType == ErrorType.NotImplemented)
                {
                    throw new NotImplementedException();
                }
                
                if (parameters.ErrorType == ErrorType.AggregateException)
                {
                    var task = Task.Factory.StartNew(() => { throw new Exception(); });
                    task.Wait();
                }
                
                if (parameters.ErrorType == ErrorType.ProcedureFailureException)
                {
                    throw new ProcedureFailureException();
                }
                
                if (parameters.ErrorType == ErrorType.ProcessExecutionException)
                {
                    throw new ProcessExecutionException(1, "");
                }
            }
        }
        
        private class TestProcParams : ProcedureParameters
        {
            public ErrorType? ErrorType { get; set; }
            public Type ErrorTypeToThrow { get; set; }
        }
    }
}
