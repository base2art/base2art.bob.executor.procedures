﻿namespace Base2art.Bob.Executor.Procedures.Basic
{
    using System;
    using Base2art.Bob.Executor.Procedures.Basic;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;
    using Newtonsoft.Json;

    [TestFixture]
    public class StatusPrinterProcedureFeature
    {
        [TestCase(StatusPosition.Begin, TargetExecutionStatus.Success)]
        [TestCase(StatusPosition.End, TargetExecutionStatus.Success)]
        [TestCase(StatusPosition.Begin, TargetExecutionStatus.Failure)]
        [TestCase(StatusPosition.End, TargetExecutionStatus.Failure)]
        [TestCase(StatusPosition.LifecyclePosition, TargetExecutionStatus.Failure)]
        [TestCase(StatusPosition.LifecyclePosition, TargetExecutionStatus.Failure)]
        public void ShouldExecute(StatusPosition inputPosition, TargetExecutionStatus status)
        {
            var targetParameters = new TargetExecutionParameters(
                Guid.Empty,
                "c:\\Temp\\",
                "c:\\Temp\\Artifacts",
                status,
                new LifecyclePosition());
            var logger = new InMemoryProcedureLogger();
            var proc = new StatusPrinterProcedure();
            
            var parameters = new StatusPrinterProcedureParameters
            {
                StatusPosition = inputPosition
            };
            
            proc.Execute(this.MakeLocator(), targetParameters, parameters, new ProcedureLogger(logger));
            
            logger.Output.Length.Should().Be(1);
            
            object val = logger.Output[0].Value;
            
            Console.WriteLine(JsonConvert.SerializeObject(val));
            val.As<StatusPrinterProcedureOutput>().StatusPosition.Should().Be(inputPosition);
            val.As<StatusPrinterProcedureOutput>().Status.Should().Be(status);
        }

        private ProcedureServiceLocator MakeLocator()
        {
            return new ProcedureServiceLocator(
                new Mock<IProcedureLocator>().Object, 
                new Mock<IProcedureLoggerFactory>().Object, 
                new Mock<IJsonSerializer>().Object, 
                new Mock<IDependencyIdentifierTypeLocator>().Object);
        }
    }
}




