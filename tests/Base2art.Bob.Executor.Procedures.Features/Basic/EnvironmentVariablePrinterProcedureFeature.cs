﻿namespace Base2art.Bob.Executor.Procedures.Basic
{
    using System;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class EnvironmentVariablePrinterProcedureFeature
    {
        [Test]
        public void ShouldExecute()
        {
            var serviceLocator = this.MakeLocator();
            var targetParameters = new TargetExecutionParameters(
                Guid.Empty,
                "c:\\Temp\\",
                "c:\\Temp\\Artifacts",
                TargetExecutionStatus.Success,
                new LifecyclePosition());
            var logger = new InMemoryProcedureLogger();
            var proc = new EnvironmentVariablePrinterProcedure();
            var parameters = new NullProcedureParameters();
            proc.Execute(serviceLocator, targetParameters, parameters, new ProcedureLogger(logger));
            
            logger.Output.Length.Should().Be(1);
            logger.Output[0].Value.Should().Be("TargetExecutionParameters.DefaultWorkingDirectory: 'c:\\Temp\\'");
        }

        private ProcedureServiceLocator MakeLocator()
        {
            return new ProcedureServiceLocator(
                new Mock<IProcedureLocator>().Object,
                new Mock<IProcedureLoggerFactory>().Object,
                new Mock<IJsonSerializer>().Object,
                new Mock<IDependencyIdentifierTypeLocator>().Object);
        }
    }
}
