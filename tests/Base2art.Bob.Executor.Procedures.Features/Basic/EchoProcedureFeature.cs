﻿namespace Base2art.Bob.Executor.Procedures.Basic
{
    using System;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;
    
    [TestFixture]
    public class EchoProcedureFeature
    {
        [Test]
        public void ShouldExecute()
        {
            var parameters = new EchoProcedureParameters
            {
                Message = "My Message"
            };
            
            var targetParameters = new TargetExecutionParameters(
                Guid.Empty,
                "c:\\Temp\\", 
                "c:\\Temp\\Artifacts", 
                TargetExecutionStatus.Success,
                new LifecyclePosition());
            var logger = new InMemoryProcedureLogger();
            
            var proc = new EchoProcedure();
            proc.Execute(this.MakeLocator(), targetParameters, parameters, new ProcedureLogger(logger));
            
            logger.Output.Length.Should().Be(1);
            logger.Debug.Length.Should().Be(0);
            logger.Error.Length.Should().Be(0);
            
            logger.Output[0].Value.Should().Be("My Message");
            logger.Output[0].Id.Should().NotBe(Guid.Empty);
            logger.Output[0].Date.Should().BeCloseTo(DateTimeOffset.UtcNow, 100);
        }
        
        [Test]
        public void ShouldExecute_WithNullMessage()
        {
            var parameters = new EchoProcedureParameters
            {
                Message = null
            };
            
            var targetParameters = new TargetExecutionParameters(
                Guid.Empty,
                "c:\\Temp\\", 
                "c:\\Temp\\Artifacts",
                TargetExecutionStatus.Success,
                new LifecyclePosition());
            var logger = new InMemoryProcedureLogger();
            
            var proc = new EchoProcedure();
            proc.Execute(this.MakeLocator(), targetParameters, parameters, new ProcedureLogger(logger));
            
            logger.Output.Length.Should().Be(1);
            logger.Debug.Length.Should().Be(0);
            logger.Error.Length.Should().Be(0);
            
            logger.Output[0].Value.Should().Be("<null>");
            logger.Output[0].Id.Should().NotBe(Guid.Empty);
            logger.Output[0].Date.Should().BeCloseTo(DateTimeOffset.UtcNow, 50);
        }

        private ProcedureServiceLocator MakeLocator()
        {
            return new ProcedureServiceLocator(
                new Mock<IProcedureLocator>().Object, 
                new Mock<IProcedureLoggerFactory>().Object, 
                new Mock<IJsonSerializer>().Object, 
                new Mock<IDependencyIdentifierTypeLocator>().Object);
        }
    }
}
