﻿namespace Base2art.Bob.Executor.Procedures.Basic
{
    using System;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    [TestFixture]
    public class NullProcedureFeature
    {
        [Test]
        public void ShouldExecute()
        {
            var parameters = new NullProcedureParameters { Description = "My Description" };
            var targetParameters = new TargetExecutionParameters(
                Guid.Empty,
                "c:\\Temp\\",
                "c:\\Temp\\Artifacts",
                TargetExecutionStatus.Success,
                new LifecyclePosition());
            using (var logger = new InMemoryProcedureLogger())
            {
                var proc = new NullProcedure();
                proc.Execute(this.MakeLocator(), targetParameters, parameters, new ProcedureLogger(logger));
                logger.Output.Length.Should().Be(0);
                logger.Debug.Length.Should().Be(0);
                logger.Error.Length.Should().Be(0);
            }
        }

        private ProcedureServiceLocator MakeLocator()
        {
            return new ProcedureServiceLocator(
                new Mock<IProcedureLocator>().Object,
                new Mock<IProcedureLoggerFactory>().Object,
                new Mock<IJsonSerializer>().Object,
                new Mock<IDependencyIdentifierTypeLocator>().Object);
        }
    }
}


