﻿namespace Base2art.Bob.Executor.Procedures.Basic
{
    using System;
    using Base2art.Bob.Executor.Procedures.CustomTypes;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;
    
    [TestFixture]
    public class SequenceProcedureFeature
    {
        [Test]
        public void ShouldExecute()
        {
            //            var parameters = new EchoProcedureParameters
            //            {
            //                Message = "My Message"
            //            };
            
            var targetParameters = new TargetExecutionParameters(
                Guid.Empty,
                "c:\\Temp\\",
                "c:\\Temp\\Artifacts",
                TargetExecutionStatus.Success,
                new LifecyclePosition());
            var logger = new InMemoryProcedureLogger();
            
            var proc = new SequenceProcedure();
            var parms = new SequenceProcedureParameters();
            parms.Procedures = new Base2art.Bob.Executor.Procedures.CustomTypes.ProcedureExecutionData[]
            {
                new ProcedureExecutionData
                {
                    ProcedureParameters = new DependencyIdentifier{ Organization = "Base2art", Product = "Echo-Parameters", Version = new Version("1.0.0.0") },
                    Procedure = new DependencyIdentifier{ Organization = "Base2art", Product = "Echo", Version = new Version("1.0.0.0") },
                    Parameters = new EchoProcedureParameters {Message = "First Task"}
                },
                new ProcedureExecutionData
                {
                    ProcedureParameters = new DependencyIdentifier{ Organization = "Base2art", Product = "Echo-Parameters", Version = new Version("1.0.0.0") },
                    Procedure = new DependencyIdentifier{ Organization = "Base2art", Product = "Echo", Version = new Version("1.0.0.0") },
                    Parameters = new EchoProcedureParameters{Message = "Second Task"}
                },
            };
            
            proc.Execute(this.MakeLocator(logger), targetParameters, parms, new ProcedureLogger(logger));
            
            logger.Output.Length.Should().Be(0);
            logger.Debug.Length.Should().Be(0);
            logger.Error.Length.Should().Be(0);
            
            
            logger.ChildLoggers.Count.Should().Be(2);
            logger.ChildLoggers[0].Output[0].Value.Should().Be("First Task");
            logger.ChildLoggers[0].Output[0].Id.Should().NotBe(Guid.Empty);
            logger.ChildLoggers[0].Output[0].Date.Should().BeCloseTo(DateTimeOffset.UtcNow, 50);
            
            logger.ChildLoggers[1].Output[0].Value.Should().Be("Second Task");
            logger.ChildLoggers[1].Output[0].Id.Should().NotBe(Guid.Empty);
            logger.ChildLoggers[1].Output[0].Date.Should().BeCloseTo(DateTimeOffset.UtcNow, 50);
        }
        
        
        private ProcedureServiceLocator MakeLocator(InMemoryProcedureLogger logger)
        {
            var subLogger = new Mock<IProcedureLoggerFactory>(MockBehavior.Strict);
            var @object = subLogger.Object;
            var procedureLogger = new Mock<IProcedureLocator>(MockBehavior.Strict);
            subLogger.Setup(x => x.CreateLoggerFor(It.IsAny<TargetExecutionParameters>(), It.IsAny<Guid>(), It.IsAny<DependencyIdentifier>(), It.IsAny<DependencyIdentifier>()))
                .Returns<TargetExecutionParameters, Guid, DependencyIdentifier, DependencyIdentifier>(logger.CreateSubLoggerFor);
            
            procedureLogger.Setup(x => x.GetProcedure(new DependencyIdentifier{ Organization = "Base2art", Product = "Echo", Version = new Version(1,0,0,0)}))
                .Returns(() => new EchoProcedure());
            
            var typeLocator = new Mock<IDependencyIdentifierTypeLocator>();
            
            typeLocator.Setup(x => x.FindType(It.IsAny<DependencyIdentifier>()))
                .Returns(typeof(object));
            
            var current = new object();
            var mock = new Mock<IJsonSerializer>();
            
            mock.Setup(x => x.Serialize(It.IsAny<object>()))
                .Returns("ABC")
                .Callback<object>(x => current = x);
            
            mock.Setup(x => x.Deserialize(It.IsAny<string>(), It.IsAny<Type>()))
                .Returns(() => current);
            
            return new ProcedureServiceLocator(
                procedureLogger.Object,
                @object,
                mock.Object,
                typeLocator.Object);
        }
    }
    
}
