﻿namespace Base2art.Bob.Executor.Procedures
{
    using FluentAssertions;
    using NUnit.Framework;
    
    [TestFixture]
    public class LifecyclePositionFeature
    {
        [TestCase(CleanLifecyclePhase.None, DefaultLifecyclePhase.None, SiteLifecyclePhase.None, true)]
        [TestCase(CleanLifecyclePhase.Clean, DefaultLifecyclePhase.None, SiteLifecyclePhase.None, false)]
        [TestCase(CleanLifecyclePhase.None, DefaultLifecyclePhase.GenerateSources, SiteLifecyclePhase.None, false)]
        [TestCase(CleanLifecyclePhase.None, DefaultLifecyclePhase.None, SiteLifecyclePhase.PreSite, false)]
        [TestCase(-1, DefaultLifecyclePhase.None, SiteLifecyclePhase.None, true)]
        [TestCase(CleanLifecyclePhase.None, -1, SiteLifecyclePhase.None, true)]
        [TestCase(CleanLifecyclePhase.None, DefaultLifecyclePhase.None, -1, true)]
        public void ShouldIsEmpty(CleanLifecyclePhase clean, DefaultLifecyclePhase @default, SiteLifecyclePhase site, bool isEmpty)
        {
            var pos = new LifecyclePosition
            {
                CleanLifecyclePhase = clean,
                DefaultLifecyclePhase = @default,
                SiteLifecyclePhase = site
            };
            
            pos.IsEmpty().Should().Be(isEmpty);
            
            var cpos = pos.ToLifecyclePosition();
            cpos.CleanLifecyclePhase.Should().Be(pos.CleanLifecyclePhase);
            cpos.DefaultLifecyclePhase.Should().Be(pos.DefaultLifecyclePhase);
            cpos.SiteLifecyclePhase.Should().Be(pos.SiteLifecyclePhase);
            
        }
    }
}
