﻿namespace Base2art.Bob.Executor.Procedures.CustomTypes
{
    using FluentAssertions;
    using NUnit.Framework;
    [TestFixture]
    public class PathStringFeature
    {
        
        [TestCase(null, false)]
        [TestCase("", false)]
        [TestCase("   ", false)]
        [TestCase("sjy/abc", true)]
        public void ShouldHaveValue(string input, bool expected)
        {
            var value = (PathString)input;
            value.HasValue.Should().Be(expected);
        }
        
        [Test]
        public void ShouldLoadPathString_ConvertToString()
        {
            string path = (string)new PathString("sjy/abc");
            path.Should().Be("sjy/abc");
        }
        
        [Test]
        public void ShouldLoadPathString_DefaultValue()
        {
            PathString path = null;
            path.GetValueOrDefault("sjy/abc").Should().Be("sjy/abc");
        }
        
        [TestCase(null, null)]
        [TestCase(null, "")]
        [TestCase("", null)]
        [TestCase("", "")]
        [TestCase("sjy/abc", "sjy/abc")]
        public void ShouldLoadPathString_ConvertToPathString(string input1, string input2)
        {
            PathString path = (input1);
            var expected = new PathString(input2);
            path.Should().Be(expected);
            path.Equals(1).Should().BeFalse();
            
            (path == expected).Should().BeTrue();
            (path != expected).Should().BeFalse();
            
            path.GetHashCode().Should().Be(expected.GetHashCode());
            
            string.Equals(path.ToString(), expected.ToString(), System.StringComparison.Ordinal);
            string.Equals(((IConvertible<string>)path).Convert(), ((IConvertible<string>)expected).Convert(), System.StringComparison.Ordinal);
        }
    }
}
