﻿namespace Base2art.Bob.Executor.Procedures.CustomTypes
{
    using System;
    using FluentAssertions;
    using NUnit.Framework;
    
    [TestFixture]
    public class PathStringTypeConverterFeature
    {
        [TestCase(typeof(string), true)]
        [TestCase(typeof(int), false)]
        [TestCase(typeof(float), false)]
        [TestCase(typeof(PathString), true)]
        public void CanConvertFrom_ShouldAnalize(Type inputType, bool expectedCanConvert)
        {
            var converter = new PathStringTypeConverter();
            converter.CanConvertFrom(inputType).Should().Be(expectedCanConvert);
        }
        
        [TestCase(typeof(string), true)]
        [TestCase(typeof(int), false)]
        [TestCase(typeof(float), false)]
        [TestCase(typeof(PathString), true)]
        public void CanConvertTo_ShouldAnalize(Type inputType, bool expectedCanConvert)
        {
            var converter = new PathStringTypeConverter();
            converter.CanConvertTo(inputType).Should().Be(expectedCanConvert);
        }
        
        [Test]
        public void ConvertFrom_String()
        {
            var converter = new PathStringTypeConverter();
            converter.ConvertFrom("abc").As<PathString>().Value.Should().Be("abc");
        }
        
        [Test]
        public void ConvertFrom_PathString()
        {
            var converter = new PathStringTypeConverter();
            converter.ConvertFrom(new PathString("abc")).As<PathString>().Value.Should().Be("abc");
        }
        
        [Test]
        public void ConvertFrom_Null()
        {
            var converter = new PathStringTypeConverter();
            converter.ConvertFrom(null).As<PathString>().HasValue.Should().BeFalse();
        }
        
        [Test]
        public void ConvertFrom_int()
        {
            var converter = new PathStringTypeConverter();
            
            Action abc = () => converter.ConvertFrom(12);
            abc.ShouldThrow<NotSupportedException>();
        }
    }
}
