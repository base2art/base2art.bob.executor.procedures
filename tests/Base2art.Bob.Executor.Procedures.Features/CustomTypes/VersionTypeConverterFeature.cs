﻿namespace Base2art.Bob.Executor.Procedures.CustomTypes
{
    using System;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class VersionTypeConverterFeature
    {
        [Test]
        public void ConvertFrom_Successfully()
        {
            var converter = new VersionTypeConverter();
            
            var output = converter.ConvertFrom("1.0.0.0");
            output.Should().Be(new Version(1, 0, 0, 0));
        }
        
        [Test]
        public void ConvertFrom_Failed()
        {
            var converter = new VersionTypeConverter();
            
            Action abc = () => converter.ConvertFrom("abd");
            abc.ShouldThrow<ArgumentException>();
        }
    }
}


