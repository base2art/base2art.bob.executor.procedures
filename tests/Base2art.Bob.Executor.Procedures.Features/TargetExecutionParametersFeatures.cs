﻿namespace Base2art.Bob.Executor.Procedures
{
    using System;
    using FluentAssertions;
    using NUnit.Framework;
    
    [TestFixture]
    public class TargetExecutionParametersFeatures
    {
        [Test]
        public void ShoulHaveGetters()
        {
            var cdwd = "c:\\dwd";
            var cdad = "c:\\dad";
            var status = TargetExecutionStatus.FailureOrAbort;
            var lifecyclePosition = new LifecyclePosition { DefaultLifecyclePhase = DefaultLifecyclePhase.Deploy };
            var tep = new TargetExecutionParameters(Guid.Empty, cdwd, cdad, status, lifecyclePosition);
            
            tep.DefaultArtifactDirectory.Should().Be(cdad);
            tep.DefaultWorkingDirectory.Should().Be(cdwd);
            tep.Status.Should().Be(status);
            tep.CurrentLifecyclePosition.DefaultLifecyclePhase.Should().Be(lifecyclePosition.DefaultLifecyclePhase);
        }
    }
}
